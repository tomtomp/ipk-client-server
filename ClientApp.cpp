/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "ClientApp.h"

namespace app
{
    ClientApp::ClientApp(const util::ClientArgParser &parser) :
        mParser(parser)
    {
    }

    void ClientApp::run()
    {
        // Open local socket.
        net::TcpSocket tcpSocket;

        // Get the server information
        net::HostInfo hostInfo(mParser.getHostname());

        // Connect to the server
        net::TcpConnection connection(hostInfo, mParser.getPort());
        connection.connect(tcpSocket);

        // Create a sender for our socket.
        net::TcpSender sender(tcpSocket);

        // Create a receiver for our socket.
        net::TcpReceiver receiver(tcpSocket);

        util::MessageGenerator generator;

        switch (mParser.getOpType())
        {
            case util::OperationTypes::DOWNLOAD:
                {
                    // Downloading a file from server.

                    if (mParser.getFilename().find('/') != std::string::npos)
                        throw std::runtime_error("File to download from server cannot have any \"/\" !");

                    std::ofstream file(mParser.getFilename(), std::ios::out | std::ios::binary);

                    if (!file)
                        throw std::runtime_error("Cannot open output file!");

                    std::vector<char> header = generator.generateRequest(util::MessageType::DOWNLOAD,
                                                                         mParser.getFilename());

                    // Send the request.
                    sender.sendMessage(header);

                    // Get the servers reply to out request.
                    std::vector<char> reply = receiver.receiveMessage();

                    size_t fileSize = 0;
                    util::codes::CodeType code = generator.parseReply(reply, fileSize);

                    if (code != util::codes::CodeType::OK)
                    {
                        std::cerr << "Received from server : \n" << util::codes::getCodeMessage(code) << std::endl;
                        throw std::runtime_error("Error code received from server");
                    }

                    // Send the server confirmation, that we are ready for the data.
                    std::vector<char> conf = generator.generateReply(util::codes::OK);
                    sender.sendMessage(conf);

                    // We can start receiving the data.

                    if (fileSize > 0)
                    {
                        std::vector<char> received;
                        received.reserve(cst::MAX_PACKET_SIZE);

                        size_t toDownload = fileSize;

                        while (toDownload != 0)
                        {
                            received = receiver.receiveMessage();
                            if (received.size() == 0)
                                throw std::runtime_error("Connection closed.");

                            file.write(&received[0], received.size());

                            if (static_cast<long>(toDownload) - static_cast<long>(received.size()) < 0)
                                throw std::runtime_error("Got more data than advertised");

                            toDownload -= received.size();
                        }
                    }
                }
                break;
            case util::OperationTypes::UPLOAD:
                {
                    // Uploading a file to server.
                    std::ifstream file(mParser.getFilename(), std::ios::in | std::ios::binary | std::ios::ate);

                    if (!file)
                        throw std::runtime_error("Cannot open input file!");

                    // Get only the filename, without path.
                    std::string origFilename(mParser.getFilename());
                    std::string pureFilename(
                        origFilename.cbegin() + origFilename.find_last_of('/') + 1,
                        origFilename.cend()
                    );

                    auto fileSize = file.tellg();
                    file.seekg(0, std::ios::beg);

                    std::vector<char> header = generator.generateRequest(util::MessageType::UPLOAD, pureFilename,
                                                                         true,
                                                                         static_cast<size_t>(fileSize));

                    // Send the header and wait for reply
                    sender.sendMessage(header);

                    std::vector<char> reply = receiver.receiveMessage();

                    size_t dummySize = 0;
                    util::codes::CodeType code = generator.parseReply(reply, dummySize);

                    if (code != util::codes::CodeType::OK)
                    {
                        std::cerr << "Received from server : \n" << util::codes::getCodeMessage(code) << std::endl;
                        throw std::runtime_error("Error code received from server");
                    }

                    // We can start sending the data.
                    char buffer[cst::MAX_PACKET_SIZE]{0};
                    long dataSize = 0;

                    decltype(fileSize) toSend = fileSize;
                    //std::istreambuf_iterator<char> it(file);

                    while (toSend != 0)
                    {
                        if (toSend >= cst::MAX_PACKET_SIZE)
                        {
                            file.read(buffer, cst::MAX_PACKET_SIZE);
                            dataSize = cst::MAX_PACKET_SIZE;
                            toSend -= cst::MAX_PACKET_SIZE;
                        }
                        else
                        {
                            file.read(buffer, toSend);
                            dataSize = toSend;
                            toSend = 0;
                        }

                        sender.sendMessage(buffer, static_cast<size_t>(dataSize));
                    }
                    break;
                }
            case util::OperationTypes::LIST:
            {
                // Getting a list of files from the server.

                std::vector<char> header = generator.generateRequest(util::MessageType::LIST,
                                                                     mParser.getFilename());

                // Send the request.
                sender.sendMessage(header);

                // Get the servers reply to out request.
                std::vector<char> reply = receiver.receiveMessage();

                size_t fileSize = 0;
                util::codes::CodeType code = generator.parseReply(reply, fileSize);

                if (code != util::codes::CodeType::OK)
                {
                    std::cerr << "Received from server : \n" << util::codes::getCodeMessage(code) << std::endl;
                    throw std::runtime_error("Error code received from server");
                }

                // Send the server confirmation, that we are ready for the data.
                std::vector<char> conf = generator.generateReply(util::codes::OK);
                sender.sendMessage(conf);

                // We can start receiving the data.
                std::cout << "File listing from server : " << std::endl;

                if (fileSize > 0)
                {
                    std::vector<char> received;
                    received.reserve(cst::MAX_PACKET_SIZE);

                    size_t toDownload = fileSize;

                    while (toDownload != 0)
                    {
                        received = receiver.receiveMessage();
                        if (received.size() == 0)
                            throw std::runtime_error("Connection closed.");

                        std::cout << std::string(received.cbegin(), received.cend());

                        if (static_cast<long>(toDownload) - static_cast<long>(received.size()) < 0)
                            throw std::runtime_error("Got more data than advertised");

                        toDownload -= received.size();
                    }
                }

                std::cout << "End of file listing." << std::endl;
                break;
            }
        default:
            throw std::runtime_error("Unknown operation");
            break;
        }
    }
}

