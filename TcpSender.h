/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef TCPSENDER_H
#define TCPSENDER_H

#include <vector>
#include <string>

#include "NetworkIncludes.h"

#include "TcpSocket.h"

namespace net
{
    /**
     * Used for sending messages.
     */
    class TcpSender
    {
    public:
        TcpSender(const TcpSender &other) = delete;
        TcpSender(TcpSender &&rhs) = delete;

        TcpSender(const net::TcpSocket &socket);

        size_t sendMessage(const std::string &message);
        size_t sendMessage(const std::vector<char> &message);
        size_t sendMessage(const char *arr, size_t size);
    private:
        int mSocket;
    protected:
    };
}


#endif //TCPSENDER_H
