/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include <cstring>

#include "NetworkIncludes.h"
#include "ErrorHandling.h"
#include "HostInfo.h"
#include "TcpSocket.h"
#include "TcpSocket.h"

namespace net
{
    /**
     * Starts and stores information about a single TCP connection.
     */
    class TcpConnection
    {
    public:
        TcpConnection(const TcpConnection &other) = delete;
        TcpConnection(TcpConnection &&rhs) = delete;

        /**
         * Create new TCP connection using the given HostInfo and port.
         * @param hostInfo Who to connect to.
         * @param port On what port.
         */
        TcpConnection(const net::HostInfo &hostInfo,
                      unsigned int port);

        /**
         * Connect using the information passed in constructor + the socket.
         *
         * @param socket Socket to make the connection with.
         */
        void connect(const net::TcpSocket &socket);

        bool isConnected() const
        {
            return mConnected;
        }
    private:
        bool mConnected;
        sockaddr_in mAddress;
        int mPort;
    protected:
    };
}


#endif //TCPCONNECTION_H
