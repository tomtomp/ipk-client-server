/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef TCPRECEIVER_H
#define TCPRECEIVER_H

#include <string>
#include <vector>
#include <string.h>

#include "NetworkIncludes.h"

#include "TcpSocket.h"

namespace net
{
    /**
     * Used for receiving messages.
     */
    class TcpReceiver
    {
    public:
        TcpReceiver(const TcpReceiver &other) = delete;
        TcpReceiver(TcpReceiver &&rhs) = delete;

        TcpReceiver(const net::TcpSocket &socket);

        std::vector<char> receiveMessage();

        static const size_t BUFFER_SIZE = cst::MAX_PACKET_SIZE;
    private:
        void clearBuffer();

        int mSocket;
        char mBuffer[BUFFER_SIZE + 1];
    protected:
    };
}


#endif //TCPRECEIVER_H
