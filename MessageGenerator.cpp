/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "MessageGenerator.h"

namespace util
{

    const std::vector<char> &MessageGenerator::generateRequest(MessageType type,
                                                               const std::string &filename,
                                                               bool sendFileSize,
                                                               size_t fileSize)
    {
        mRequest.erase(mRequest.begin(), mRequest.end());

        // Add the starting letter.
        switch (type)
        {
            case MessageType::DOWNLOAD:
                mRequest.push_back('D');
                break;
            case MessageType::UPLOAD:
                mRequest.push_back('U');
                break;
            case MessageType::LIST:
                mRequest.push_back('L');
                break;
            default:
                throw std::runtime_error("Unknown message type!");
        }

        mRequest.push_back('=');

        // Copy the filename to the message.
        std::copy(filename.cbegin(), filename.end(), std::back_inserter(mRequest));

        mRequest.push_back('\n');

        // If the file size was specified, add it to the message.
        if (sendFileSize)
        {
            mRequest.push_back('S');
            mRequest.push_back('=');

            std::string sizeStr = std::to_string(fileSize);

            std::copy(sizeStr.begin(), sizeStr.end(), std::back_inserter(mRequest));
            mRequest.push_back('\n');
        }

        mRequest.push_back('\0');

        return mRequest;
    }

    std::string MessageGenerator::parseRequest(const std::vector<char> &request,
                                               MessageType &type,
                                               size_t &dataSize)
    {
        if (request.size() < 4)
        {
            type = MessageType::UNKNOWN;
            return "";
        }

        // Parse the first letter.
        switch (request[0])
        {
            case 'U':
                type = MessageType::UPLOAD;
                break;
            case 'D':
                type = MessageType::DOWNLOAD;
                break;
            case 'L':
                type = MessageType::LIST;
                break;
            default:
                type = MessageType::UNKNOWN;
                return "";
        }

        // request[1] = '='

        // Get the filename/mask.
        std::stringstream ss;
        int index = 2;
        while (request[index] != '\n' && index < static_cast<int>(request.size()))
            ss << request[index++];
        std::string filename(ss.str());

        if (filename.find('/') != std::string::npos)
        {
            type = MessageType::ERR;
            return "";
        }

        // request[index] is now in the '\n', there should be at least one more char '\0'
        if (index == static_cast<int>(request.size()))
        {
            type = MessageType::UNKNOWN;
            return "";
        }

        if (type == MessageType::UPLOAD)
        {
            // There should be a file size here too.
            index++;
            if (request[index] != 'S')
            {
                type = MessageType::UNKNOWN;
                return "";
            }
            index++;
            // request[index] = '='

            // Atleast one digit, '\n' and '\0'
            if (index + 3 > static_cast<int>(request.size()))
            {
                type = MessageType::UNKNOWN;
                return "";
            }

            index++;
            ss.str(std::string());
            ss.clear();
            while (request[index] != '\n')
            {
                if (request[index] < '0' || request[index] > '9')
                {
                    type = MessageType ::UNKNOWN;
                    return "";
                }
                ss << request[index];
                index++;
            }
            ss >> dataSize;

            /*
            if (dataSize == 0)
            {
                type = MessageType ::UNKNOWN ;
                return "";
            }
             */
        }

        return filename;
    }

    const std::vector<char> &MessageGenerator::generateReply(codes::TCode code,
                                                             bool sendFileSize,
                                                             size_t fileSize)
    {
        mReply.erase(mReply.begin(), mReply.end());

        mReply.push_back('E');
        mReply.push_back('=');

        mReply.push_back(code[0]);
        mReply.push_back(code[1]);
        mReply.push_back('\n');

        // If the function user specified a fileSize.
        if (sendFileSize)
        {
            mReply.push_back('S');
            mReply.push_back('=');
            std::string strFileSize = std::to_string(fileSize);
            std::copy(strFileSize.begin(), strFileSize.end(), std::back_inserter(mReply));
            mReply.push_back('\n');
        }

        mReply.push_back('\0');

        return mReply;
    }

    codes::CodeType MessageGenerator::parseReply(const std::vector<char> &reply,
                                                 size_t &dataSize)
    {
        // The smallest reply is 6 bytes.
        if (reply.size() < 6)
            return codes::CodeType::UNK;

        if (reply[0] != 'E')
            return codes::CodeType::UNK;
        // reply[1] = '='

        // Parse the code
        if (reply[2] < '0' || reply[2] > '9')
            return codes::CodeType::UNK;
        if (reply[3] < '0' || reply[3] > '9')
            return codes::CodeType::UNK;
        codes::TCode strCode{reply[2], reply[3]};
        codes::CodeType messageCode = codes::getNumCode(strCode);

        // Parse the data size
        if (reply[5] == 'S')
        {
            // Size atleast 9
            if (reply.size() < 9)
                return codes::CodeType::UNK;
            // reply[6] = '='

            std::stringstream ss;
            int index = 7;
            while (reply[index] != '\n')
            {
                if (reply[index] < '0' || reply[index] > '9')
                    throw std::runtime_error("Found unknown character in the size attribute");
                ss << reply[index];
                index++;
            }
            ss >> dataSize;
        }

        return messageCode;
    }
}
