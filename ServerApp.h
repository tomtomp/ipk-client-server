/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef SERVERAPP_H
#define SERVERAPP_H

#include <vector>
#include <fstream>
#include <thread>
#include <mutex>
#include <functional>

#include "NetworkIncludes.h"
#include "ServerArgParser.h"
#include "TcpSocket.h"
#include "TcpReceiver.h"
#include "TcpSender.h"
#include "ServerThreadPool.h"

namespace app
{
    /**
     * Main server application.
     */
    class ServerApp
    {
    public:
        ServerApp(const ServerApp &other) = delete;
        ServerApp(ServerApp &&rhs) = delete;

        /**
         * Get information from given argument parser.
         *
         * @param parser Argument parser.
         */
        ServerApp(const util::ServerArgParser &parser);

        /**
         * Main application loop.
         */
        void run();

        /**
         * Parse the request message from client.
         * @param request Request we received.
         * @param dataSize Will contain the size of data to receive, if the request is upload request.
         * @param type Will contain the type of the request.
         * @return Name of the file.
         */
        //static std::string parseRequest(const std::vector<char> &request, RequestType &type, size_t &dataSize);

        /**
         * Generate reply message.
         * @param errorCode Code to send to client.
         * @param Size of the file we will be sending.
         * @return The reply message.
         */
        //static std::vector<char> createReply(const char* errorCode, size_t fileSize=0);
    private:
        /**
         * Fill the socket address structure.
         */
        void fillSocketAddress();

        /**
         * Used for signal interrupt handling.
         */
        static void sigHandler(int);

        const util::ServerArgParser &mParser;
        sockaddr_in mSocketAddress;

        static std::atomic_bool mRunning;
    protected:
    };
} //app namespace


#endif //SERVERAPP_H
