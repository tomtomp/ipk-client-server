/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "TcpSocket.h"

namespace net
{
    TcpSocket::TcpSocket() :
        mSocketNum(0)
    {
        //if ((mSocketNum = socket(AF_INET, SOCK_STREAM, 0)) <= 0)
        //std::cout << "Using IPPROTO_TCP" << std::endl;
        if ((mSocketNum = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) <= 0)
            throw std::runtime_error("Unable to open client socket!");
        int enable = 1;
        if (setsockopt(mSocketNum, SOL_SOCKET,  SO_REUSEADDR, &enable, sizeof(int)) < 0)
            throw std::runtime_error("Unable to set socket reusable!");
    }

    TcpSocket::TcpSocket(uint16_t portNumber) :
        mSocketNum(portNumber)
    {
    }

    TcpSocket::TcpSocket(TcpSocket &&rhs)
    {
        std::swap(rhs.mSocketNum, this->mSocketNum);
    }

    void TcpSocket::bindTo(const sockaddr_in &sockAddr)
    {
        if (bind(mSocketNum, reinterpret_cast<const sockaddr*>(&sockAddr), sizeof(sockAddr)) < 0)
            throw std::runtime_error("Unable to bind socket!");
    }

    void TcpSocket::listenOn(int backlog)
    {
        if (listen(mSocketNum, backlog) < 0)
            throw std::runtime_error("Unable to listen on socket!");
    }

    uint16_t TcpSocket::acceptConnection(sockaddr_in &sockAddrClient, socklen_t &sockAddrClientLength)
    {
        // Set the timeout for 100ms.
        timeval timeout{0};
        timeout.tv_sec = 0;
        timeout.tv_usec = 100000;
        fd_set set;
        FD_ZERO(&set);
        FD_SET(mSocketNum, &set);
        int commSocket = 0;

        int selectRet = select(mSocketNum + 1, &set, NULL, NULL, &timeout);
        if (selectRet > 0)
            commSocket = accept(mSocketNum, reinterpret_cast<sockaddr*>(&sockAddrClient),
                                &sockAddrClientLength);

        return static_cast<uint16_t>(commSocket);
    }

    void TcpSocket::setNonBlocking()
    {
        int flags = fcntl(mSocketNum, F_GETFL, 0);
        if (fcntl(mSocketNum, F_SETFL, flags | O_NONBLOCK) < 0)
            throw std::runtime_error("Unable to set socket as non-blocking!");
    }

    void TcpSocket::setBlocking()
    {
        int flags = fcntl(mSocketNum, F_GETFL, 0);
        if (fcntl(mSocketNum, F_SETFL, flags & ~O_NONBLOCK) < 0)
            throw std::runtime_error("Unable to set socket as blocking!");
    }

    TcpSocket::~TcpSocket()
    {
        if (mSocketNum)
            close(mSocketNum);
    }
}
