/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef MESSAGEGENERATOR_H
#define MESSAGEGENERATOR_H

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <string>
#include <cassert>

namespace util
{
    /**
     * Types of messages.
     */
    enum class MessageType
    {
        DOWNLOAD = 0,
        UPLOAD,
        LIST,
        ERR,
        UNKNOWN
    };

    namespace codes
    {
        using TCode = const char[2];

        TCode OK{'0', '0'};

        /* Codes for communication when client is uploading */
        TCode FILE_WR_OPEN_FAILED{'1', '1'};
        TCode FILE_WRITE_FAIL{'1', '2'};

        /* Codes for communication when client is downloading */
        TCode FILE_RD_OPEN_FAILED{'2', '1'};
        TCode FILE_READ_FAIL{'2', '2'};

        TCode ERR{'8', '8'};
        TCode UNK{'9', '9'};

        enum class CodeType
        {
            OK = 0,

            FILE_WR_OPEN_FAILED = 11,
            FILE_WRITE_FAIL = 12,

            FILE_RD_OPEN_FAILED = 21,
            FILE_READ_FAIL,

            ERR = 98,
            UNK
        };

        inline const char *getCodeMessage(CodeType code);
        inline const char *getCodeMessageStr(TCode code);
        inline const char *getStrCode(CodeType code);
        inline CodeType getNumCode(TCode code);

        inline const char *getCodeMessage(CodeType code)
        {
            switch (code)
            {
                case CodeType::OK:
                    return "Everything is OK";
                case CodeType::FILE_WR_OPEN_FAILED:
                    return "Cannot open the file for writing";
                case CodeType::FILE_WRITE_FAIL:
                    return "Cannot write to file";
                case CodeType::FILE_RD_OPEN_FAILED:
                    return "Cannot open the file for reading";
                case CodeType::FILE_READ_FAIL:
                    return "Cannot read the file";
                case CodeType::ERR:
                    return "Error in the last message";
                default:
                    return "Unknown code";
            }
        }

        inline const char *getCodeMessageStr(TCode code)
        {
            CodeType codeType = static_cast<CodeType>(getNumCode(code));
            return getCodeMessage(codeType);
        }

        inline const char *getStrCode(CodeType code)
        {
            switch (code)
            {
                case CodeType::OK:
                    return OK;
                case CodeType::FILE_WR_OPEN_FAILED:
                    return FILE_WR_OPEN_FAILED;
                case CodeType::FILE_WRITE_FAIL:
                    return FILE_WRITE_FAIL;
                case CodeType::FILE_RD_OPEN_FAILED:
                    return FILE_RD_OPEN_FAILED;
                case CodeType::FILE_READ_FAIL:
                    return FILE_READ_FAIL;
                case CodeType::ERR:
                    return ERR;
                default:
                    return UNK;
            }
        }

        inline CodeType getNumCode(TCode code)
        {
            assert(code[0] >= '0' && code[0] <= '9');
            assert(code[1] >= '0' && code[1] <= '9');

            uint16_t firstDig = static_cast<uint16_t>(code[0] - '0');
            uint16_t secondDig = static_cast<uint16_t>(code[1] - '0');

            switch (static_cast<uint16_t>(firstDig * 10 + secondDig))
            {
                case 0:
                    return CodeType::OK;
                case 11:
                    return CodeType::FILE_WR_OPEN_FAILED;
                case 12:
                    return CodeType::FILE_WRITE_FAIL;
                case 21:
                    return CodeType::FILE_RD_OPEN_FAILED;
                case 22:
                    return CodeType::FILE_READ_FAIL;
                case 88:
                    return CodeType::ERR;
                default:
                    return CodeType::UNK;
            }
        }

    }

    /**
     * Class used for generating messages.
     */
    class MessageGenerator
    {
    public:
        MessageGenerator(const MessageGenerator &other) = delete;
        MessageGenerator(MessageGenerator &&rhs) = delete;

        /**
         * This is a static class, no specific initializers required.
         */
        MessageGenerator()
        {}

        /**
         * Generate a request message containing given information.
         * @param type Type of the message.
         * @param filename Name of the file/mask.
         * @param fileSize Optional parameter when uploading a file.
         * @return Message in character vector.
         */
        const std::vector<char> &generateRequest(MessageType type,
                                                 const std::string &filename,
                                                 bool sendFileSize = false,
                                                 size_t fileSize = 0);

        /**
         * Parse the given request.
         * @param request The request to parse.
         * @param type Type of the message will be returned to this variable.
         * @param dataSize If the re is a data size in this request, it will be stored in this variable.
         * @return Returns the filename/mask.
         */
        std::string parseRequest(const std::vector<char> &request,
                                 MessageType &type,
                                 size_t &dataSize);

        /**
         * Get already created request.
         * @return The request.
         */
        const std::vector<char> &getRequest()
        {
            return mRequest;
        }

        /**
         * Generate a reply message containing given information.
         * @param code Code to insert to the message. Codes are available in the util::codes namespace.
         * @param fileSize Optional file size when client is downloading a file.
         * @return Message in character vector.
         */
        const std::vector<char> &generateReply(codes::TCode code,
                                               bool sendFileSize = false,
                                               size_t fileSize = 0);

        /**
         * Parse the given reply.
         * @param reply Reply to parse.
         * @param dataSize If the size attribute is contained inside the reply, this variable will be filled with
         *  value of this attribute.
         * @return Code from the message.
         */
        codes::CodeType parseReply(const std::vector<char> &reply,
                                   size_t &dataSize);

        /**
         * Get already created reply.
         * @return The reply.
         */
        const std::vector<char> &getReply()
        {
            return mReply;
        }

    private:
        std::vector<char> mRequest;
        std::vector<char> mReply;
    protected:
    };
}


#endif //MESSAGEGENERATOR_H
