/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "TcpSender.h"

namespace net
{
    TcpSender::TcpSender(const net::TcpSocket &socket) :
        mSocket(socket.getSocket())
    {
    }

    size_t TcpSender::sendMessage(const std::string &message)
    {
        std::string::size_type size = message.size();
        ssize_t sent = 0;
        if ((sent = send(mSocket, message.c_str(), size, 0)) < static_cast<long>(size))
            throw std::runtime_error("Unable to send the whole message!");
        return static_cast<size_t>(sent);
    }

    size_t TcpSender::sendMessage(const std::vector<char> &message)
    {
        const char *cMessage = &message.front();
        std::string::size_type size = message.size();
        ssize_t sent = 0;
        if ((sent = send(mSocket, cMessage, size, 0)) < static_cast<long>(size))
            throw std::runtime_error("Unable to send the whole message!");
        return static_cast<size_t>(sent);
    }

    size_t TcpSender::sendMessage(const char *arr, size_t size)
    {
        ssize_t sent = 0;
        if ((sent = send(mSocket, arr, size, 0)) < static_cast<long>(size))
            throw std::runtime_error("Unable to send the whole message!");
        return static_cast<size_t>(sent);
    }
}
