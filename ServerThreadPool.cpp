/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "ServerThreadPool.h"

namespace util
{
    ServerThreadPool::ServerThreadPool(unsigned int poolSize) :
        mLock(), mThreadBarrier(poolSize + 1, mLock)
    {
        mWorkersPool.reserve(poolSize);

        // Create the threads.
        for (unsigned int iii = 0; iii < poolSize; ++iii)
        {
            // Make them wait before all of them are created.
            mWorkQueue.push([this] () -> int {
                mThreadBarrier.wait();
                return 0;
            });

            // Add the new thread to the thread pool
            mWorkersPool.emplace_back(std::thread(&ServerThreadPool::workerMain, this));
        }

        // Release all of the threads from wait.
        mThreadBarrier.wait();
    }

    void ServerThreadPool::workerMain()
    {
        bool running = true;
        while (running)
        {
            auto work = getWork();

            try
            {
                work();
            }
            catch (TerminateThread &e)
            {
                running = false;
                break;
            }
            catch (std::runtime_error &e)
            {
                std::cout << "Error occurred in one of the worker Threads :\n" << e.what() << std::endl;
            }
            catch (...)
            {
                // Kill all other exceptions.
            }

        }
    }

    std::function<int()> ServerThreadPool::getWork()
    {
        std::unique_lock<std::mutex> lock(mLock);
        // Wait until there is some work to do.
        mWorkCondition.wait(lock, [this] () { return !this->mWorkQueue.empty(); });
        std::function<int()> work = mWorkQueue.front();
        mWorkQueue.pop();
        return work;
    }

    void ServerThreadPool::addWork(std::function<int()> work)
    {
        std::unique_lock<std::mutex> lock(mLock);
        mWorkQueue.push(work);
        mWorkCondition.notify_one();
    }

    void ServerThreadPool::stopAllThreads()
    {
        std::unique_lock<std::mutex> lock(mLock);

        // Terminate all of the threads.
        for (decltype(mWorkersPool.size()) iii = 0; iii < mWorkersPool.size(); ++iii)
        {
            mWorkQueue.push([] () -> int {
                throw TerminateThread();
            });
            mWorkCondition.notify_one();
        }
    }

    template<typename T>
    void ServerThreadPool::tsPrint(std::initializer_list<T> args)
    {
        std::unique_lock<std::mutex> lock(mLock);

        for (auto arg : args)
            std::cout << arg;
        std::cout << std::endl;
    }

    ServerThreadPool::~ServerThreadPool()
    {
        stopAllThreads();
        mWorkCondition.notify_all();
        for (std::thread &th : mWorkersPool)
            th.join();
    }

    ThreadBarrier::ThreadBarrier(uint count, std::mutex &lock) :
        mLock(lock), mCounter(count)
    {
    }

    void ThreadBarrier::wait()
    {
        std::unique_lock<std::mutex> lock(mLock);

        if (--mCounter > 0)
            mBarrier.wait(lock, [this] () { return mCounter <= 0;});
        else
            mBarrier.notify_all();
    }
}
