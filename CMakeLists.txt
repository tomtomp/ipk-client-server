cmake_minimum_required(VERSION 3.3)
project(projekt2)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -Wall -g -std=c++14 -lpthread")
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/bin)

set(SOURCE_FILES_CLIENT client.cpp ClientApp.cpp ClientArgParser.cpp ErrorHandling.cpp TcpReceiver.cpp TcpSender.cpp
        TcpSocket.cpp HostInfo.cpp TcpConnection.cpp MessageGenerator.cpp LockableFile.cpp)
add_executable(client ${SOURCE_FILES_CLIENT})

set(SOURCE_FILES_SERVER server.cpp ServerApp.cpp ServerArgParser.cpp ErrorHandling.cpp TcpSocket.cpp TcpSender.cpp
        TcpReceiver.cpp ServerThreadPool.cpp MessageGenerator.cpp LockableFile.cpp)
add_executable(server ${SOURCE_FILES_SERVER})
