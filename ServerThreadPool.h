/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef SERVERTHREADPOOL_H
#define SERVERTHREADPOOL_H

#include <iostream>
#include <exception>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <functional>
#include <queue>
#include <vector>

namespace util
{
    /**
     * Used as a barrier with given group size.
     */
    class ThreadBarrier
    {
    public:
        ThreadBarrier(const ThreadBarrier &other) = delete;
        ThreadBarrier(ThreadBarrier &&rhs) = delete;

        /**
         * Create the thread barrier with given count.
         * @param count How many waits have to occur.
         */
        ThreadBarrier(uint count, std::mutex &lock);

        /**
         * Keeps the thread here, until the number of threads that come here is count.
         */
        void wait();
    private:
        std::mutex &mLock;
        int mCounter;
        std::condition_variable mBarrier;
    protected:
    };

    /**
     * Inspired by : http://codereview.stackexchange.com/questions/47122/a-simple-thread-pool
     */
    class ServerThreadPool
    {
    public:
        ServerThreadPool() = delete;
        ServerThreadPool(const ServerThreadPool &other) = delete;

        /**
         * Create the thread pool with poolSize threads.
         * @param poolSize Number of threads in the thread pool.
         */
        ServerThreadPool(unsigned int poolSize);

        /**
         * Add the given work item to the work queue.
         * @param work Work item.
         */
        void addWork(std::function<int()> work);

        /**
         * Stop all of the threads.
         */
        void stopAllThreads();

        ~ServerThreadPool();
    private:

        /**
         * Special exception used for ending thread loops.
         */
        class TerminateThread : public std::exception
        {
        public:
            TerminateThread() :
                std::exception()
            {
            }
        private:
        protected:
        };

        /**
         * Main function for the worker threads.
         */
        void workerMain();

        /**
         * Thread safe debug print.
         */
        template<typename T>
        void tsPrint(std::initializer_list<T> args);

        /**
         * Get the next work item.
         * @return Function object.
         */
        std::function<int()> getWork();

        std::mutex mLock;
        std::vector<std::thread> mWorkersPool;
        std::queue<std::function<int()>> mWorkQueue;
        std::condition_variable mWorkCondition;
        ThreadBarrier mThreadBarrier;
    protected:
    };
}


#endif //SERVERTHREADPOOL_H
