/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include <stdexcept>
#include "ClientApp.h"
#include "ClientArgParser.h"
#include "ErrorHandling.h"

int main(int argc, char *argv[])
{
    try
    {
        util::ClientArgParser argParser(argc, argv);

        app::ClientApp newApp(argParser);

        try
        {
            newApp.run();
        }
        catch (std::runtime_error error)
        {
            err::exit(err::Error::NETWORK, error.what());
        }
    }
    catch (std::runtime_error error)
    {
        err::exit(err::Error::PARAM, error.what());
    }

    return 0;
}
