/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef CLIENT_APP_H
#define CLIENT_APP_H

#include <vector>
#include <fstream>
#include <algorithm>

#include "ClientArgParser.h"
#include "MessageGenerator.h"
#include "HostInfo.h"
#include "TcpReceiver.h"
#include "TcpConnection.h"
#include "TcpSender.h"
#include "TcpSocket.h"

namespace app
{
    /**
     * Main client application.
     */
    class ClientApp
    {
    public:
        ClientApp(const ClientApp &other) = delete;
        ClientApp(ClientApp &&rhs) = delete;

        /**
         * Get information from given argument parser.
         *
         * @param parser Argument parser.
         */
        ClientApp(const util::ClientArgParser &parser);

        /**
         * Main application loop.
         */
        void run();
    private:
        /**
         * Get message header with given information.
         * @param type Type of the message.
         * @param filename Name of the file.
         * @return The message header as character vector.
         */
        //std::vector<char> getHeader(MessageType type, const std::string &filename, size_t fileSize=0);

        /**
         * Parse the reply message for an upload request.
         * @param reply The message received from server.
         * @return Code found in the message.
         */
        //ErrorCode parseUploadReply(const std::vector<char> &reply);

        /**
         * Parse the reply message for a download request.
         * @param reply The message received from server.
         * @return Code found in the message.
         */
        //ErrorCode parseDownloadReply(const std::vector<char> &reply, size_t &dataSize);

        const util::ClientArgParser &mParser;
    protected:
    };
} //app namespace

#endif //CLIENT_APP_H
