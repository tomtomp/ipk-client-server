/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef HOSTINFO_H
#define HOSTINFO_H

#include <string>

#include "NetworkIncludes.h"
#include "ErrorHandling.h"

namespace net
{
    class HostInfo {
    public:
        HostInfo(const HostInfo &other) = delete;
        HostInfo(HostInfo &&rhs) = delete;

        /**
         * Get the information about host.
         */
        HostInfo(const std::string &host);

        /**
         *  Hostname getter
         *  @return Hostname string.
         */
        std::string getHostName() const
        {
            return mHostName;
        }

        /**
         * Host info strutrure getter.
         * @return Pointer to the structure.
         */
        const hostent *getHostInfo() const
        {
            return mpHostInfo;
        }

        /**
         * Get IP as string.
         * @return String version of IP address.
         */
        std::string getIpString() const
        {
            return mIpString;
        }
    private:
        std::string mHostName;
        hostent *mpHostInfo;
        std::string mIpString;
    protected:
    };
}


#endif //HOSTINFO_H
