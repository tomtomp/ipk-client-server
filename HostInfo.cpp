/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "HostInfo.h"

namespace net
{
    HostInfo::HostInfo(const std::string &host) :
        mHostName(host), mpHostInfo(nullptr)
    {
        if ((mpHostInfo = gethostbyname(mHostName.c_str())) == nullptr)
            throw std::runtime_error("Unable to get server info!");

        char buffer[16] = {0, };
        if (!inet_ntop(AF_INET, *mpHostInfo->h_addr_list, buffer, 16))
            //err::exit(err::Error::NETWORK, "Unable to convert IP to string!");
            ;
        mIpString = std::string(buffer);
    }
}
