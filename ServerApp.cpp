/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "ServerApp.h"
#include "MessageGenerator.h"

namespace app
{
    std::atomic_bool ServerApp::mRunning(true);

    ServerApp::ServerApp(const util::ServerArgParser &parser) :
        mParser(parser)
    {
        fillSocketAddress();
    }

    void ServerApp::run()
    {
        // Open local socket.
        net::TcpSocket welcomeSocket;
        welcomeSocket.setNonBlocking();

        // Bind the socket
        welcomeSocket.bindTo(mSocketAddress);

        // Listen on socket, backlog is 10.
        welcomeSocket.listenOn(10);

        sockaddr_in clientSocket{0, };
        socklen_t clientSocketLen{0};
        util::ServerThreadPool threadPool(8);

        auto originalHandler = signal(SIGINT, ServerApp::sigHandler);

        while (mRunning)
        {
            // Wait for connection
            uint16_t commSocket = welcomeSocket.acceptConnection(clientSocket, clientSocketLen);

            if (commSocket > 0)
            {
                threadPool.addWork([commSocket] () {
                    // Create object wrapper for communication port.
                    net::TcpSocket socket(commSocket);
                    socket.setBlocking();

                    // Create message receiver for client port.
                    net::TcpReceiver receiver(socket);

                    // Create message sender for client port.
                    net::TcpSender sender(socket);

                    // Create message generator.
                    util::MessageGenerator generator;

                    // Get the clients request.
                    std::vector<char> request = receiver.receiveMessage();

                    if (request.size() == 0)
                        return 0;

                    util::MessageType type = util::MessageType::UNKNOWN;
                    size_t dataSize = 0;
                    std::string filename = generator.parseRequest(request, type, dataSize);

                    switch (type)
                    {
                        case util::MessageType::DOWNLOAD:
                        {
                            if (util::ServerArgParser::isFolder(filename))
                            {
                                sender.sendMessage(generator.generateReply(util::codes::FILE_WR_OPEN_FAILED));
                                throw std::runtime_error("Cannot send a folder");
                            }

                            // Uploading file to a client.
                            std::ifstream file(filename, std::ios::in | std::ios::binary | std::ios::ate);

                            if (!file)
                            {
                                sender.sendMessage(generator.generateReply(util::codes::FILE_WR_OPEN_FAILED));
                                throw std::runtime_error("Cannot open requested file");
                            }

                            // Get the file size.
                            auto fileSize = file.tellg();
                            file.seekg(0, std::ios::beg);

                            // Generate reply to clients request.
                            std::vector<char> header = generator.generateReply(util::codes::OK,
                                                                               true,
                                                                               static_cast<size_t>(fileSize));

                            // Send the reply.
                            size_t sent = sender.sendMessage(header);
                            if (sent != header.size())
                                throw std::runtime_error("Connection closed");

                            // Get confirmation by client.
                            std::vector<char> conf = receiver.receiveMessage();
                            if (conf.size() == 0)
                                throw std::runtime_error("Connection closed");

                            size_t dummySize = 0;
                            util::codes::CodeType confCode = generator.parseReply(conf, dummySize);
                            if (confCode != util::codes::CodeType::OK)
                                throw std::runtime_error("Client does not want to proceed with the upload");

                            // We can start sending the data.
                            char buffer[cst::MAX_PACKET_SIZE]{0};
                            long blockSize = 0;

                            decltype(fileSize) toSend = fileSize;

                            while (toSend != 0)
                            {
                                if (toSend >= cst::MAX_PACKET_SIZE)
                                {
                                    file.read(buffer, cst::MAX_PACKET_SIZE);
                                    blockSize = cst::MAX_PACKET_SIZE;
                                    toSend -= cst::MAX_PACKET_SIZE;
                                }
                                else
                                {
                                    file.read(buffer, toSend);
                                    blockSize = toSend;
                                    toSend = 0;
                                }

                                sent = sender.sendMessage(buffer, static_cast<size_t>(blockSize));
                                if (static_cast<long>(sent) != blockSize)
                                    throw std::runtime_error("Connection closed");
                            }
                            break;
                        }
                        case util::MessageType::UPLOAD:
                        {
                            // Downloading a file from a client.
                            std::ofstream file(filename, std::ios::out | std::ios::binary);

                            if (!file)
                            {
                                sender.sendMessage(generator.generateReply(util::codes::FILE_RD_OPEN_FAILED));
                                throw std::runtime_error("Cannot open requested file");
                            }

                            std::vector<char> header = generator.generateReply(util::codes::OK);

                            // Send the reply
                            sender.sendMessage(header);

                            // We can start receiving the data.

                            if (dataSize > 0) {
                                std::vector<char> received;
                                received.reserve(cst::MAX_PACKET_SIZE);

                                size_t toDownload = dataSize;

                                while (toDownload != 0) {
                                    received = receiver.receiveMessage();
                                    if (received.size() == 0)
                                        throw std::runtime_error("Connection closed");

                                    file.write(&received[0], received.size());

                                    if (static_cast<long>(toDownload) - static_cast<long>(received.size()) < 0)
                                        throw std::runtime_error("Got more data than advertised");

                                    toDownload -= received.size();
                                }
                            }
                            else
                                throw std::runtime_error("No data to receive");

                            break;
                        }
                        case util::MessageType::LIST:
                        {
                            std::stringstream ss;

                            DIR *dir;
                            struct dirent *fileEntity;

                            if((dir  = opendir(".")) == NULL)
                            {
                                sender.sendMessage(generator.generateReply(util::codes::FILE_READ_FAIL));
                                return 0;
                            }

                            while ((fileEntity = readdir(dir)) != NULL)
                            {
                                if (strcmp(fileEntity->d_name, "server") &&
                                    strcmp(fileEntity->d_name, "..") &&
                                    strcmp(fileEntity->d_name, "."))
                                    ss << fileEntity->d_name << "\n";
                            }
                            closedir(dir);

                            // Generate reply to clients request.
                            std::vector<char> header = generator.generateReply(util::codes::OK,
                                                                               true,
                                                                               static_cast<size_t>(ss.str().size()));

                            // Send the reply.
                            size_t sent = sender.sendMessage(header);
                            if (sent != header.size())
                                throw std::runtime_error("Connection closed");

                            // Get confirmation by client.
                            std::vector<char> conf = receiver.receiveMessage();
                            if (conf.size() == 0)
                                throw std::runtime_error("Connection closed");

                            size_t dummySize = 0;
                            util::codes::CodeType confCode = generator.parseReply(conf, dummySize);
                            if (confCode != util::codes::CodeType::OK)
                                throw std::runtime_error("Client does not want to proceed with the upload");

                            sender.sendMessage(ss.str());
                            break;
                        }
                        case util::MessageType::ERR:
                        {
                            sender.sendMessage(generator.generateReply(util::codes::ERR));
                            break;
                        }
                        default:
                            sender.sendMessage(generator.generateReply(util::codes::UNK));
                            break;
                    }

                    return 0;
                });
            }
        }

        std::cout << "Stopping threads" << std::endl;
        threadPool.stopAllThreads();
        std::cout << "Stopped threads" << std::endl;

        signal(SIGINT, originalHandler);
    }

    void ServerApp::fillSocketAddress()
    {
        memset(&mSocketAddress, 0, sizeof(mSocketAddress));
        mSocketAddress.sin_family = AF_INET;
        mSocketAddress.sin_addr.s_addr = INADDR_ANY;
        mSocketAddress.sin_port = htons(mParser.getPort());
    }

    void ServerApp::sigHandler(int)
    {
        ServerApp::mRunning = false;
    }
}
