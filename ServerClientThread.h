/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef SERVERCLIENTTHREAD_H
#define SERVERCLIENTTHREAD_H

#include <iostream>
#include <thread>
#include <atomic>

#include <cstdint>

namespace app
{
    /**
     * Server thread to communicate with client.
     */
    class ServerClientThread
    {
    public:
        ServerClientThread() = delete;
        ServerClientThread(const ServerClientThread &other) = delete;

        /**
         * Create new thread with given port number.
         *
         * @param portNum Number of the port to communicate through.
         */
        ServerClientThread(uint16_t portNum);

        /**
         * Star the thread.
         */
        void start();

        /**
         * Stop the thread.
         */
        ~ServerClientThread();
    private:
        /**
         * Main thread loop.
         */
        void threadMain();

        uint16_t mPort;
        bool mDestructed;
        std::thread mThisThread;
        static std::atomic_int mThreadsAlive;
    protected:
    };
}


#endif //SERVERCLIENTTHREAD_H
