/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef SERVER_ARG_PARSER_H
#define SERVER_ARG_PARSER_H

extern "C"
{
#include <sys/types.h>
#include <sys/stat.h>
}

#include <iostream>
#include <exception>
#include <string>
#include <sstream>

#include <cstring>

namespace util
{
    /**
     * Argument parsing class.
     */
    class ServerArgParser
    {
    public:
        ServerArgParser(const ServerArgParser &other) = delete;
        ServerArgParser(ServerArgParser &&rhs) = delete;

        /**
         * Parse given arguments.
         * @param argc Number of arguments.
         * @param argv Argument vector.
         */
        ServerArgParser(int argc, char *argv[]);

        /**
         * Get port number.
         * @return Port number.
         */
        uint16_t getPort() const
        {
            return static_cast<uint16_t>(mPort);
        }

        /**
         * Print the help information.
         */
        void printHelp() const;

        static bool isFolder(const std::string &filepath);

    private:
        unsigned int mPort = 0;
    protected:
    };
} //util namespace


#endif //SERVER_ARG_PARSER_H
