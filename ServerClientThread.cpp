/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "ServerClientThread.h"

namespace app
{
    ServerClientThread::ServerClientThread(uint16_t portNum) :
        mPort(portNum), mDestructed(false)
    {
    }

    void ServerClientThread::start()
    {
        mThisThread = std::thread(&ServerClientThread::threadMain, this);
    }

    void ServerClientThread::threadMain()
    {
        std::cout << "Hello from thread! : " << mPort << std::endl;
        for (int iii = 0; iii < 20; ++iii)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            std::cout << "Doing work : " << iii << " " << mDestructed << " " << mPort << std::endl;
        }
    }

    ServerClientThread::~ServerClientThread()
    {
        std::cout << "Destructing thread... " << mPort << std::endl;
        mDestructed = true;
        if (mThisThread.joinable())
            mThisThread.join();
        std::cout << "End of thread destruction." << std::endl;
    }
}
