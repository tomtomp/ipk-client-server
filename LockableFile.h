/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef LOCKABLEFILE_H
#define LOCKABLEFILE_H

#include <cstdio>

namespace util
{
    /**
     * File wrapper allowing the locking of files.
     */
    class LockableFile
    {
    public:
        LockableFile(const LockableFile &other) = delete;
        LockableFile(LockableFile &&other) = delete;

        /**
         * Open the file using given information.
         * @param filename Filename.
         * @param mode Mode to open the file under.
         */
        LockableFile(const char *filename, const char *mode);

        /**
         * Lock
         */
        void lock(int );
        void unlock();

    private:
        FILE *mFile = nullptr;
    protected:
    };
}


#endif //LOCKABLEFILE_H
