/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "ServerArgParser.h"

namespace util
{
    ServerArgParser::ServerArgParser(int argc, char **argv)
    {
        if (argc != 3)
        {
            printHelp();
            throw std::runtime_error("Wrong number of arguments");
        }

        // client -p 10000

        if (std::strcmp(argv[1], "-p"))
        {
            printHelp();
            throw std::runtime_error("Second switch has to be -p");
        }
        char *endptr = nullptr;
        unsigned long portNum = std::strtoul(argv[2], &endptr, 10);
        if (portNum > 65535 || *endptr != '\0')
            throw std::runtime_error("Port number has to be in <0, 65535>");
        mPort = static_cast<uint16_t>(portNum);
    }

    void ServerArgParser::printHelp() const
    {
        std::cout << "Usage : \n"
                  << "  ./server -p <PORT>"
                  << std::endl;
    }

    bool ServerArgParser::isFolder(const std::string &filepath)
    {
        struct stat pathStat{0, };
        stat(filepath.c_str(), &pathStat);
        return !S_ISREG(pathStat.st_mode);
    }
}
