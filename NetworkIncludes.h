/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef NETWORKINCLUDES_H
#define NETWORKINCLUDES_H

extern "C"
{
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>
#include <dirent.h>
}

namespace cst
{
    const long MAX_PACKET_SIZE = 2000;
}

#endif //NETWORKINCLUDES_H