/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include <stdexcept>
#include "ClientArgParser.h"

namespace util
{
    ClientArgParser::ClientArgParser(int argc, char **argv)
    {
        if (argc != 7)
        {
            printHelp();
            throw std::runtime_error("Wrong number of arguments");
        }

        // client -h eva.fit.vutbr.cz -p 10000 -u test.txt

        if (std::strcmp(argv[1], "-h"))
        {
            printHelp();
            throw std::runtime_error("First switch has to be -h");
        }
        mHostname = std::string(argv[2]);

        if (std::strcmp(argv[3], "-p"))
        {
            printHelp();
            throw std::runtime_error("Second switch has to be -p");
        }
        char *endptr = nullptr;
        unsigned long portNum = std::strtoul(argv[4], &endptr, 10);
        if (portNum > 65535 || *endptr != '\0')
            throw std::runtime_error("Port number has to be in <0, 65535>");
        mPort = static_cast<uint16_t>(portNum);

        if (!std::strcmp(argv[5], "-d"))
            mOpType = OperationTypes::DOWNLOAD;
        else if (!std::strcmp(argv[5], "-u"))
            mOpType = OperationTypes::UPLOAD;
        else if (!std::strcmp(argv[5], "-l"))
            mOpType = OperationTypes::LIST;
        else
        {
            printHelp();
            throw std::runtime_error("Third switch has to be -d or -u");
        }

        mFilename = std::string(argv[6]);
        if (mFilename.length() > 100)
            throw std::runtime_error("Filename is too long!");

        if (isFolder(mFilename) && mOpType == OperationTypes::UPLOAD)
            throw std::runtime_error("Cannot upload a folder!");

        if (mOpType == OperationTypes::LIST && mFilename != "*")
            throw std::runtime_error("Only supported mask at the moment is \"*\" .");
    }

    void ClientArgParser::printHelp() const
    {
        std::cout << "Usage : \n"
                  << "  ./client -h <HOSTNAME | IP> -p <PORT> -d|-u|-l <FILE_PATH | FILENAME | FILE_MASK>"
                  << std::endl;
    }

    bool ClientArgParser::isFolder(const std::string &filepath)
    {
        struct stat pathStat{0, };
        stat(filepath.c_str(), &pathStat);
        return !S_ISREG(pathStat.st_mode);
    }
}
