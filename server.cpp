/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#include "ServerApp.h"
#include "ServerArgParser.h"
#include "ErrorHandling.h"

int main(int argc, char *argv[])
{
    try
    {
        util::ServerArgParser argParser(argc, argv);

        app::ServerApp newApp(argParser);

        try
        {
            newApp.run();
        }
        catch (std::runtime_error error)
        {
            err::exit(err::Error::NETWORK, error.what());
        }
    }
    catch (std::runtime_error error)
    {
        err::exit(err::Error::PARAM, error.what());
    }

    return 0;
}
