/*
 * Task IPK - ClientServer: Client server file transfer application in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef CLIENT_ARG_PARSER_H
#define CLIENT_ARG_PARSER_H

extern "C"
{
#include <sys/types.h>
#include <sys/stat.h>
}

#include <iostream>
#include <exception>
#include <string>
#include <sstream>

#include <cstring>

namespace util
{

    enum class OperationTypes
    {
        UPLOAD,
        DOWNLOAD,
        LIST,
        UNK
    };

    /**
     * Argument parsing class.
     */
    class ClientArgParser
    {
    public:
        ClientArgParser(const ClientArgParser &other) = delete;
        ClientArgParser(ClientArgParser &&rhs) = delete;

        /**
         * Parse given arguments.
         * @param argc Number of arguments.
         * @param argv Argument vector.
         */
        ClientArgParser(int argc, char *argv[]);

        /**
         * Get copy of hostname.
         * @return String containing hostname.
         */
        std::string getHostname() const
        {
            return mHostname;
        }

        /**
         * Get copy of filename.
         * @return String containing filename.
         */
        std::string getFilename() const
        {
            return mFilename;
        }

        /**
         * Get port number.
         * @return Port number.
         */
        unsigned int getPort() const
        {
            return mPort;
        }

        /**
         * Was the -d argument passed?
         * @return Returns true, if -d was passed as argument.
         */
        bool isDownload() const
        {
            return mOpType == OperationTypes::DOWNLOAD;
        }

        /**
         * Get the operation type.
         * @return Returns the type of requested operation.
         */
        OperationTypes getOpType() const
        {
            return mOpType;
        }

        /**
         * Print the help message.
         */
        void printHelp() const;
    private:

        static bool isFolder(const std::string &filepath);

        std::string mHostname;
        std::string mFilename;
        unsigned int mPort = 0;
        OperationTypes mOpType;
    protected:
    };
} //util namespace


#endif //CLIENT_ARG_PARSER_H
