/*
 * Task IPK - webclient: Download client in C++ for IPK 2015/2016
 * Name and surname: Tomáš Polášek
 * Login: xpolas34
 */

#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include "NetworkIncludes.h"
#include "ErrorHandling.h"

namespace net
{
    /**
     * Starts and stores information about a single TCP socket.
     */
    class TcpSocket
    {
    public:
        TcpSocket(const TcpSocket &other) = delete;

        /**
         * Create a new TCP socket.
         */
        TcpSocket();

        /**
         * Move constructor.
         */
        TcpSocket(TcpSocket &&rhs);

        /**
         * Use given port identifier to construct TcpSocket object.
         * @param portNumber Port number of !!active!! port.
         */
        TcpSocket(uint16_t portNumber);

        /**
         * Get socket identifier.
         * @return Socket file descriptor.
         */
        int getSocket() const
        {
            return mSocketNum;
        }

        /**
         * Bind this socket to the given information.
         * @param sockAddr Reference to a socket address structure.
         */
        void bindTo(const sockaddr_in &sockAddr);

        /**
         * Listen on this port for connections.
         * @param backlog Size of waiting queue.
         */
        void listenOn(int backlog);

        /**
         * Try to connect, if failed, return 0.
         * @param sockAddrClient Reference to a structure, which will be filled with information about client.
         * @param sockAddrClientLength Reference to length of data in sockAddrClient.
         * @return Communication socket number.
         */
        uint16_t acceptConnection(sockaddr_in &sockAddrClient, socklen_t &sockAddrClientLength);

        /**
         * Set this socket as non-blocking.
         */
        void setNonBlocking();

        /**
         * Set this socket as blocking.
         */
        void setBlocking();

        ~TcpSocket();
    private:
        int mSocketNum;
    protected:
    };
}


#endif //TCPSOCKET_H
